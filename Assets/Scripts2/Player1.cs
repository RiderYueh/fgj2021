using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player1 : MonoBehaviour
{

    public int PlayerNum;

    public GameObject go_playerSprite;
    public GameObject go_monsterSprite;

    public Animator m_Animator;

    private Rigidbody2D rb2D;
    public float speed;
    private float realSpeed;

    public bool isGetItem;

    public string m_W;
    public string m_S;
    public string m_A;
    public string m_D;
    public string m_Q;
    public string m_E;

    public bool isMonster;
    public float monsterTime = 5;
    private float monsterTimer;

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        realSpeed = speed;
        monsterTimer = monsterTime;
    }

    private Vector2 velocity;
    private float attackTime = 0;
    private void Update()
    {
        CheckKey();
        CheckKey2();
        CheckDistance();

        if(isGhosting)
        {
            realSpeed -= Time.deltaTime;
        }
        if(realSpeed <= 0)
        {
            go_playerSprite.SetActive(false);
            go_monsterSprite.SetActive(true);
            //m_SR.sprite = monsterSprite[PlayerNum];
            realSpeed = speed;
            isGhosting = false;
            isMonster = true;
            AudioManager.instance.PlaySound("howl");

            isGetItem = false;
            Destroy(tempItem);
            tempItem = null;
            GameProject3.instance.RemoveAllGhost();
        }

        if(isMonster)
        {
            monsterTimer -= Time.deltaTime;
            if(monsterTimer <= 0)
            {
                monsterTimer = monsterTime;
                isMonster = false;
                isGhost = false;
                go_playerSprite.SetActive(true);
                go_monsterSprite.SetActive(false);
                GameProject3.instance.m_GhostTimerController.StartCount();
            }
        }
    }

    void CheckKey()
    {
        float tempY = 0;
        float tempX = 0;
        if (Input.GetKey(m_W))
        {
            tempY = realSpeed;
            wallState = 1;
            m_Animator.SetInteger("State", 1);
        }
        else if (Input.GetKey(m_S))
        {
            tempY = -realSpeed;
            wallState = 0;
            m_Animator.SetInteger("State", 0);
        }
        else
        {
            tempY = 0;
        }

        if (Input.GetKey(m_A))
        {
            tempX = -realSpeed;
            wallState = 2;
            m_Animator.SetInteger("State", 2);
            if(transform.localScale.x < 0)
            {
                transform.localScale = new Vector3(-transform.localScale.x , transform.localScale.y , transform.localScale.z);
            }
        }
        else if (Input.GetKey(m_D))
        {
            tempX = realSpeed;
            wallState = 3;
            m_Animator.SetInteger("State", 2);
            if (transform.localScale.x > 0)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            }
        }
        else
        {
            tempX = 0;
        }

        velocity = new Vector2(tempX, tempY);
    }

    int wallState = 0;//down top left right
    void CheckKey2()
    {
        if(!isMonster)
        {
            if (Input.GetKeyDown(m_Q))
            {
                if (tempItem != null && !isGetItem)
                {
                    isGetItem = true;
                    AudioManager.instance.PlaySound("pick");
                }
            }

            if (Input.GetKeyDown(m_E))
            {
                if (isGetItem)
                {
                    
                    GameObject go = null;
                    AudioManager.instance.PlaySound("wall");
                    isGetItem = false;
                    Destroy(tempItem);
                    tempItem = null;
                    if (wallState == 0)
                    {
                        go = Instantiate(GameProject3.instance.wallPrefab2, GameProject3.instance.wallParent.transform) as GameObject;
                        go.transform.localPosition = transform.localPosition + new Vector3(0, -1, 0);
                    }
                    else if(wallState == 1)
                    {
                        go = Instantiate(GameProject3.instance.wallPrefab2, GameProject3.instance.wallParent.transform) as GameObject;
                        go.transform.localPosition = transform.localPosition + new Vector3(0, 1, 0);
                    }
                    else if (wallState == 2)
                    {
                        go = Instantiate(GameProject3.instance.wallPrefab, GameProject3.instance.wallParent.transform) as GameObject;
                        go.transform.localPosition = transform.localPosition + new Vector3(-1, 0, 0);
                    }
                    else if (wallState == 3)
                    {
                        go = Instantiate(GameProject3.instance.wallPrefab, GameProject3.instance.wallParent.transform) as GameObject;
                        go.transform.localPosition = transform.localPosition + new Vector3(1, 0, 0);
                    }
                }
            }
        }
        else
        {
            attackTime -= Time.deltaTime;
            if (attackTime <=0 )
            {
                if (Input.GetKeyDown(m_Q))
                {
                    attackTime = 0.5f;
                    AudioManager.instance.PlaySound("attack");
                    GameObject go = Instantiate(GameProject3.instance.attackPrefab, GameProject3.instance.wallParent.transform) as GameObject;
                    Destroy(go, 0.1f);
                    if (wallState == 2)
                    {
                        go.transform.localPosition = transform.localPosition + new Vector3(-0.5f, 0, 0);
                    }
                    else 
                    {
                        go.transform.localPosition = transform.localPosition + new Vector3(0.5f, 0, 0);
                    }
                }
            }
        }
        
    }

    void CheckDistance()
    {
        if (Vector3.Distance(transform.localPosition, GameProject3.instance.castle[PlayerNum].transform.localPosition) < 1)
        {
            if(Input.GetKeyDown(m_Q))
            {
                if(isGetItem)
                {
                    AudioManager.instance.PlaySound("hammer");
                    isGetItem = false;
                    Destroy(tempItem);
                    tempItem = null;
                    GameProject3.instance.castle[PlayerNum].SetCastleProgress();
                }
                
            }
        }
    }

    void FixedUpdate()
    {
        rb2D.MovePosition(rb2D.position + velocity * Time.fixedDeltaTime);

        if(tempItem != null && isGetItem)
        {
            tempItem.transform.position = transform.position;
        }
    }

    GameObject tempItem;
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (isGetItem)
            return;
        if (collision.tag == "Item")
        {
            tempItem = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isGetItem)
            return;
        tempItem = null;
    }

    bool isGhosting;
    bool isGhost;
    public void OnGhost()
    {
        if (isGhost) return;
        GameProject3.instance.isStartGhost = false; //���β���
        isGhost = true;
        //m_SR.sprite = monsterSprite[PlayerNum];
        isGhosting = true;
        AudioManager.instance.PlaySound("hehe");
    }

}
