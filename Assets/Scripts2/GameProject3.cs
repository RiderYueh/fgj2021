using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameProject3 : MonoBehaviour
{
    static public GameProject3 instance;
    public GameObject endUI;
    public Text endText;
    public bool isStartGhost;
    public GameObject itemPrefab;
    public GameObject itemParent;
    public GameObject wallPrefab;
    public GameObject wallPrefab2;
    public GameObject wallParent;

    public GameObject attackPrefab;

    public int maxItem;

    public Player1 player1;
    public Player1 player2;

    public GhostAI ghostPrefab;
    public GameObject ghostParent;

    public MainCastleController[] castle;
    public GhostTimerController m_GhostTimerController;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    private void Start()
    {
        CreatItem();
        m_GhostTimerController.StartCount();
    }

    
    float goGhostTimer = 0;
    int ghostCount = 0;
    private void Update()
    {

        if (Input.GetKeyDown("1"))
        {
            SceneManager.LoadScene(0);
        }
        if (Input.GetKeyDown("2"))
        {
            SceneManager.LoadScene(1);
        }

        if (Input.GetKeyDown("="))
        {
            Time.timeScale += 1;
        }
        if (Input.GetKeyDown("-"))
        {
            Time.timeScale = 1;
        }

        if (isStartGhost)
        {
            goGhostTimer -= Time.deltaTime;
            if (goGhostTimer <=0)
            {
                goGhostTimer = 4;
                Instantiate(ghostPrefab.gameObject, ghostParent.transform);
                Instantiate(ghostPrefab.gameObject, ghostParent.transform);
                ghostCount += 2;
            }

            if(ghostCount >= 20)
            {
                isStartGhost = false;
            }
        }
       
    }

    public void StartGhost()
    {
        AudioManager.instance.PlaySound("ghost");
        isStartGhost = true;
    }

    void CreatItem()
    {
        Invoke("CreatItem", 60);
        float maxY = 6.5f;
        float minY = -4.5f;
        float maxX = 10f;
        float minX = -10f;

        for (int i = 0; i < maxItem; i++)
        {
            GameObject go = Instantiate(itemPrefab, itemParent.transform) as GameObject;
            go.transform.localPosition = new Vector3(Random.Range(minX , -1) , Random.Range(minY , maxY));
        }

        for (int i = 0; i < maxItem; i++)
        {
            GameObject go = Instantiate(itemPrefab, itemParent.transform) as GameObject;
            go.transform.localPosition = new Vector3(Random.Range(1, maxX), Random.Range(minY, maxY));
        }
    }

    public void RemoveAllGhost()
    {
        foreach(Transform child in ghostParent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void Player1Win(bool isWin)
    {
        StartCoroutine(GoWin(isWin));
    }

    IEnumerator GoWin(bool isWin)
    {
        yield return new WaitForSeconds(1);
        endUI.SetActive(true);
        if (isWin)
        {
            endText.text = "P1 is Winner\nPress 1 or 2 to restart";
        }
        else
        {
            endText.text = "P2 is Winner\nPress 1 or 2 to restart";
        }
    }
    
}
