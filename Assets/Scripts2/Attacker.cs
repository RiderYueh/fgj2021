using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            //Player1 p1 = collision.gameObject.GetComponent<Player1>();
        }
        if (collision.tag == "Wall")
        {
            Wall wall = collision.gameObject.GetComponent<Wall>();
            wall.m_hp -= 1;
            if(wall.m_hp == 0)
            {
                Destroy(wall.gameObject);
                AudioManager.instance.PlaySound("wallbreak");
            }
        }

        if(collision.tag == "Castle")
        {
            MainCastleController mcc = collision.gameObject.GetComponent<MainCastleController>();
            foreach(MainCastleController tempMcc in GameProject3.instance.castle)
            {
                if(tempMcc != mcc)
                {
                    AudioManager.instance.PlaySound("steal");
                    if (mcc.m_progress >= 2)
                    {
                        tempMcc.SetCastleProgress(2);
                        mcc.SetCastleProgress(-2);
                    }
                    else
                    {
                        tempMcc.SetCastleProgress(mcc.m_progress);
                        mcc.SetCastleProgress(-mcc.m_progress);
                    }
                    
                }
            }
        }

    }
}
