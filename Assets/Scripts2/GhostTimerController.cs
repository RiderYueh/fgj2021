using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class GhostTimerController : MonoBehaviour
{
    private int GhostSecond = 20;
    private Sequence seq;

    [SerializeField] TextMeshProUGUI textGhostGhostSecond;

    public void StartCount()
    {
        GhostSecond = 20;
        textGhostGhostSecond.text = GhostSecond.ToString();

        textGhostGhostSecond.gameObject.SetActive(true);
        InvokeRepeating("GhostTimer", 1, 1);
    }

    private void GhostTimer()
    {
        GhostSecond -= 1;

        seq = DOTween.Sequence();

        if (GhostSecond >= 10)
            seq.Append(textGhostGhostSecond.gameObject.transform.DOScale(new Vector3(2.0f, 2.0f, 2.0f), 0.15f));
        if (GhostSecond < 10)
            seq.Append(textGhostGhostSecond.gameObject.transform.DOScale(new Vector3(3.0f, 3.0f, 3.0f), 0.15f));
        seq.Insert(0.15f, textGhostGhostSecond.gameObject.transform.DOScale(new Vector3(1.0f, 1.0f, 1.0f), 0.15f));
        
        textGhostGhostSecond.text = GhostSecond.ToString();

        if (GhostSecond == 0)
        {
            textGhostGhostSecond.gameObject.SetActive(false);
            GameProject3.instance.StartGhost();
            seq.Kill();
            CancelInvoke("GhostTimer");
        }
    }
}
