using UnityEngine;

public class MainCastleController : MonoBehaviour
{
    // 主建築物進度
    private int buildProgress;
    private readonly int buildMaxProgress = 100;

    [SerializeField] SpriteRenderer spriteRendererCastle;

    [SerializeField] Sprite spriteCastle1;
    [SerializeField] Sprite spriteCastle2;
    [SerializeField] Sprite spriteCastle3;
    [SerializeField] Sprite spriteCastle4;
    [SerializeField] Sprite spriteCastle5;

    [SerializeField] ProgressBarController progressBarController;

    void Start()
    {
        buildProgress = 0;
        spriteRendererCastle.sprite = spriteCastle1;

        progressBarController.SetMaxProgress(buildMaxProgress);
        progressBarController.SetProgress(buildProgress);
    }

    public int m_progress = 0;
    public void SetCastleProgress(int _progress = 5)
    {
        this.m_progress += _progress;
        progressBarController.SetProgress(m_progress);

        if (m_progress < 25)
            spriteRendererCastle.sprite = spriteCastle1;
        else if (m_progress >= 25 && m_progress < 50)
            spriteRendererCastle.sprite = spriteCastle2;
        else if (m_progress >= 50 && m_progress < 75)
            spriteRendererCastle.sprite = spriteCastle3;
        else if (m_progress >= 75 && m_progress < 100)
            spriteRendererCastle.sprite = spriteCastle4;
        else if (m_progress >= 100)
            spriteRendererCastle.sprite = spriteCastle5;

        if(m_progress >= 100)
        {
            if(this == GameProject3.instance.castle[0])
            {
                GameProject3.instance.Player1Win(true);
            }
            else
            {
                GameProject3.instance.Player1Win(false);
            }
            
        }
    }
}
