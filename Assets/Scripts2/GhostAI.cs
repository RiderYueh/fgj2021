using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAI : MonoBehaviour
{
    public float speed;

    float timer = 1;

    private void Update()
    {
        if(player != null)
        {
            transform.localPosition = player.transform.localPosition;
        }
        TimeTick();
        CheckState();
        CheckBorder();
    }

    void TimeTick()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = Random.Range(0, 3);
        }
    }

    float moveTime = 1;
    float dirX;
    float dirY;
    void CheckState()
    {
        moveTime -= Time.deltaTime;

        if (moveTime <= 0)
        {
            timer = 0;
            moveTime = Random.Range(0, 0.5f);
            dirX = Random.Range(-1, 2);
            dirY = Random.Range(-1, 2);

            for (int i = 0; i < 2; i++)
            {
                Transform target = null;
                if(i == 0)
                {
                    target = GameProject3.instance.player1.transform;
                }
                if (i == 1)
                {
                    target = GameProject3.instance.player2.transform;
                }
                if (Vector3.Distance(transform.localPosition, target.localPosition) < 3)
                {
                    if (transform.localPosition.x > target.localPosition.x && dirX > 0)
                    {
                        dirX = Random.Range(-1, 2);
                        if(dirX > 0)
                        {
                            dirX = Random.Range(-1, 2);
                        }
                    }
                    else if (transform.localPosition.x < target.localPosition.x && dirX < 0)
                    {
                        dirX = Random.Range(-1, 2);
                        if (dirX < 0)
                        {
                            dirX = Random.Range(-1, 2);
                        }
                    }

                    if (transform.localPosition.y > target.localPosition.y && dirY > 0)
                    {
                        dirY = Random.Range(-1, 2);
                        if (dirY > 0)
                        {
                            dirY = Random.Range(-1, 2);
                        }
                    }
                    else if (transform.localPosition.y < target.localPosition.y && dirY < 0)
                    {
                        dirY = Random.Range(-1, 2);
                        if (dirY < 0)
                        {
                            dirY = Random.Range(-1, 2);
                        }
                    }
                }
            }

            
        }
        transform.position += new Vector3(Time.deltaTime * dirX * speed, Time.deltaTime * dirY * speed, 0);
    }

    void CheckBorder()
    {
        if (transform.localPosition.x < -10)
        {
            dirX = 1;
        }
        if (transform.localPosition.x > 10)
        {
            dirX = -1;
        }
        if (transform.localPosition.y > 6)
        {
            dirY = -1;
        }
        if (transform.localPosition.y < -4)
        {
            dirY = 1;
        }
    }

    GameObject player;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Player1 p1 = collision.gameObject.GetComponent<Player1>();
            p1.OnGhost();
            player = p1.gameObject;
        }
    }
}
