using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampPlayer : MonoBehaviour
{
    public float speed;
    public GameObject bullet;

    static public TrampPlayer instnace;

    private void Awake()
    {
        if (instnace == null)
        {
            instnace = this;
        }
    }

    void Update()
    {
        CheckKey();
        CheckMouse();
    }

    float dirX;
    float dirY;
    void CheckKey()
    {

        if (Input.GetKey("i"))
        {
            if(transform.localPosition.y < 6)
                dirY = 1;
            else
                dirY = 0;
        }
        else if (Input.GetKey("k"))
        {
            if (transform.localPosition.y > -4)
                dirY = -1;
            else
                dirY = 0;
        }
        else
        {
            dirY = 0;
        }

        if (Input.GetKey("j"))
        {
            if (transform.localPosition.x > -9)
                dirX = -1;
            else
                dirX = 0;
        }
        else if (Input.GetKey("l"))
        {
            if (transform.localPosition.x  < 9)
                dirX = 1;
            else
                dirX = 0;
        }
        else
        {
            dirX = 0;
        }
        transform.position += new Vector3(dirX * Time.deltaTime * speed, dirY * Time.deltaTime * speed, 0);
    }

    float dodgeTimer;
    float fireTimer;
    void CheckMouse()
    {
        dodgeTimer -= Time.deltaTime;
        fireTimer -= Time.deltaTime;
        
        if(Input.GetKeyDown(KeyCode.U))
        {
            if (fireTimer < 0)
            {
                if(dirX != 0 || dirY != 0)
                {
                    GameObject go = Instantiate(bullet, transform.parent) as GameObject;
                    BulletMove bm = go.GetComponent<BulletMove>();
                    bm.Init(dirX, dirY);
                    Destroy(go, 3);
                    go.transform.position = transform.position;
                    fireTimer = 3;
                    AudioManager.instance.PlaySound("gun");
                }
                
            }
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            if (dodgeTimer < 0)
            {
                transform.position += new Vector3(dirX * Time.deltaTime * 500, dirY * Time.deltaTime * 500, 0);
                dodgeTimer = 3;
            }
            
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Pooh")
        {
            Destroy(gameObject);
            GameManager.instance.PoohWin();
        }
        if (collision.tag == "PoohPlayer")
        {
            Destroy(gameObject);
            GameManager.instance.PoohWin();
        }
    }
}
