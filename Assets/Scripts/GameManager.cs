using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    static public GameManager instance;
    public GameObject allPlayer;

    public GameObject m_player;
    public GameObject m_pooh;
    public GameObject m_poohPlayer;

    public bool isChoosingPoohPlayer;
    public bool isTrampWin;

    public int poohWinCount = 0;
    public int trampWinCount = 0;

    private void Awake()
    {
        if(instance == null)
            instance = this;
    }

    private void Start()
    {
        Reset();
    }

    public void Reset()
    {
        isChoosingPoohPlayer = false;
        isTrampWin = false;
        MainUI.instance.Reset();
        foreach (Transform child in allPlayer.transform)
        {
            Destroy(child.gameObject);
        }

        Instantiate(m_player, allPlayer.transform);
        Instantiate(m_poohPlayer, allPlayer.transform);

        for (int i = 0; i < 10; i++)
        {
            Instantiate(m_pooh, allPlayer.transform);
        }
    }

    void Update()
    {
        if(Input.GetKeyDown("="))
        {
            Time.timeScale += 1;
        }
        if (Input.GetKeyDown("-"))
        {
            Time.timeScale = 1;
        }

        if (Input.GetKeyDown("1"))
        {
            SceneManager.LoadScene(0);
        }
        if (Input.GetKeyDown("2"))
        {
            SceneManager.LoadScene(1);
        }
    }

    public void PoohWin()
    {
        Debug.Log("PoohWin");
        isChoosingPoohPlayer = true;
        MainUI.instance.ActiveChooseText();
    }

    public void PoohRealWin()
    {
        MainUI.instance.SetWin(true);
        poohWinCount += 1;
        isChoosingPoohPlayer = false;
        StartCoroutine(DelayReset());
    }

    public void TrampWin()
    {
        isTrampWin = true;
        isChoosingPoohPlayer = false;
        trampWinCount += 1;
        Debug.Log("TrampWin");
        MainUI.instance.SetWin(false);
        StartCoroutine(DelayReset());
    }

    IEnumerator DelayReset()
    {
        yield return new WaitForSeconds(3);

        if (trampWinCount >= 3)
        {
            MainUI.instance.TrampWin(true);
        }
        else if (poohWinCount >= 3)
        {
            MainUI.instance.TrampWin(false);
        }
        else
        {
            Reset();
        }
    }
}
