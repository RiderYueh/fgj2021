using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    public float speed;
    public float dirX;
    public float dirY;


    public void Init(float _dirX , float _dirY)
    {
        dirX = _dirX;
        dirY = _dirY;

        if(dirX < 0 && dirY > 0)
        {
            transform.Rotate(0, 0, 45);
        }
        else if (dirX < 0 && dirY == 0)
        {
            transform.Rotate(0, 0, 90);
        }
        else if (dirX < 0 && dirY < 0)
        {
            transform.Rotate(0, 0, 135);
        }
        else if (dirX == 0 && dirY < 0)
        {
            transform.Rotate(0, 0, 180);
        }

        else if (dirX > 0 && dirY < 0)
        {
            transform.Rotate(0, 0, 225);
        }
        else if (dirX > 0 && dirY == 0)
        {
            transform.Rotate(0, 0, 270);
        }
        else if (dirX > 0 && dirY > 0)
        {
            transform.Rotate(0, 0, 315);
        }

    }

    void Update()
    {
        transform.position += new Vector3(dirX * Time.deltaTime* speed, dirY * Time.deltaTime* speed, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Pooh")
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        if (collision.tag == "PoohPlayer")
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
            GameManager.instance.TrampWin();
        }
    }
}
