using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AudioStruct
{
    public AudioClip clip;
    public string name;
}

public class AudioManager : MonoBehaviour
{
    static public AudioManager instance;
    public List<AudioStruct> list_AllAudio;
    public AudioSource mainAudioSource;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void PlaySound(string name , Transform objParent = null)
    {
        foreach(AudioStruct tempAS in list_AllAudio)
        {
            if(tempAS.name == name)
            {
                mainAudioSource.PlayOneShot(tempAS.clip);
                return;
            }
        }
    }
}
