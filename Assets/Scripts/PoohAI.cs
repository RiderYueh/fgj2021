using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoohAI : MonoBehaviour
{
    public float speed;

    float timer = 1;

    bool isStop;
    bool isCallStop;
    private void Update()
    {
        if(isStop || GameManager.instance.isTrampWin)
        {
            return;
        }

        if(!isCallStop && GameManager.instance.isChoosingPoohPlayer)
        {
            isCallStop = true;
            StartCoroutine(RandomStop());
        }

        TimeTick();
        CheckState();
        CheckBorder();
    }

    IEnumerator RandomStop()
    {
        yield return new WaitForSeconds(Random.Range(0,0.9f));
        isStop = true;
    }

    void TimeTick()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = Random.Range(0,3);
        }
    }

    float moveTime = 1;
    float dirX;
    float dirY;
    void CheckState()
    {
        moveTime -= Time.deltaTime;
        
        if (moveTime <= 0)
        {
            timer = 0;
            moveTime = Random.Range(0,0.5f);
            dirX = Random.Range(-1,2);
            dirY = Random.Range(-1, 2);

            if(TrampPlayer.instnace != null)
            {
                if (transform.localPosition.x > TrampPlayer.instnace.transform.localPosition.x && dirX > 0)
                {
                    dirX = Random.Range(-1, 2);
                }
                else if (transform.localPosition.x < TrampPlayer.instnace.transform.localPosition.x && dirX < 0)
                {
                    dirX = Random.Range(-1, 2);
                }

                if (transform.localPosition.y > TrampPlayer.instnace.transform.localPosition.y && dirY > 0)
                {
                    dirY = Random.Range(-1, 2);
                }
                else if (transform.localPosition.y < TrampPlayer.instnace.transform.localPosition.y && dirY < 0)
                {
                    dirY = Random.Range(-1, 2);
                }
            }
        }
        transform.position += new Vector3(Time.deltaTime * dirX * speed, Time.deltaTime * dirY * speed, 0);
    }

    void CheckBorder()
    {
        if(transform.localPosition.x < -11)
        {
            dirX = 1;
        }
        if (transform.localPosition.x > 11)
        {
            dirX = -1;
        }
        if (transform.localPosition.y > 7)
        {
            dirY = -1;
        }
        if (transform.localPosition.y < -5)
        {
            dirY = 1;
        }
    }
}
