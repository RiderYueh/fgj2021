using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoonPlayer : MonoBehaviour
{
    static public PoonPlayer instnace;
    public float speed;

    private void Awake()
    {
        if(instnace == null)
        {
            instnace = this;
        }
    }


    void Update()
    {
        CheckKey();
    }

    float dirX;
    float dirY;
    void CheckKey()
    {
        if(Input.GetKeyDown("q"))
        {
            if(transform.localScale.x == 2)
            {
                transform.localScale = new Vector3(1,1, 1);
            }
            else
            {
                transform.localScale = new Vector3(2, 2, 1);
                AudioManager.instance.PlaySound("hehehe");
            }
        }

        if(Input.GetKey("w"))
        {
            dirY = 1;
        }
        else if (Input.GetKey("s"))
        {
            dirY = -1;
        }
        else
        {
            dirY = 0;
        }

        if (Input.GetKey("a"))
        {
            dirX = -1;
        }
        else if (Input.GetKey("d"))
        {
            dirX = 1;
        }
        else
        {
            dirX = 0;
        }
        transform.position += new Vector3(dirX * Time.deltaTime * speed, dirY * Time.deltaTime * speed, 0);
    }
}
