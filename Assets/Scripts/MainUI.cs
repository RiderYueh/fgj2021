using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUI : MonoBehaviour
{
    static public MainUI instance;
    public GameObject trampPrefab;
    public GameObject poohPrefab;
    public GameObject m_parent;

    public Text m_chooseText;

    private void Awake()
    {
        if(instance == null)
            instance = this;
        
    }

    private void Update()
    {
        if(GameManager.instance.isChoosingPoohPlayer)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

                if (hit.collider != null)
                {
                    if(hit.collider.tag == "PoohPlayer")
                    {
                        SetChooseText(false);
                        GameManager.instance.TrampWin();
                    }
                    else
                    {
                        SetChooseText(true);
                        GameManager.instance.PoohRealWin();
                    }
                }
            }
        }
    }

    public void SetWin(bool isPooh)
    {
        if(isPooh)
        {
            GameObject go = Instantiate(poohPrefab, m_parent.transform) as GameObject;
            go.SetActive(true);
        }
        else
        {
            GameObject go = Instantiate(trampPrefab, m_parent.transform) as GameObject;
            go.SetActive(true);
        }
    }

    public void SetChooseText(bool isFail)
    {
        if(isFail)
        {
            m_chooseText.text = "P1 Win";
        }
        else
        {
            m_chooseText.text = "P2 Win";
        }
    }

    public void ActiveChooseText()
    {
        m_chooseText.gameObject.SetActive(true);
    }

    public void Reset()
    {
        m_chooseText.gameObject.SetActive(false);
        m_chooseText.text = "Choose a Pooh";
    }

    public void TrampWin(bool isWin)
    {
        if(isWin)
        {
            m_chooseText.text = "P2 is Winnnnnnnnnnnnnnnnnner\n Press 1 or 2 Chose Other Game";
        }
        else
        {
            m_chooseText.text = "P1 is Winnnnnnnnnnnnnnnnnner\n Press 1 or 2 Chose Other Game";
        }
    }
}
